package TestCase;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import CommonFunctions.OpenBrowser;
import ObjectsandPageElements.LoginPageElements;
import jxl.read.biff.BiffException;

public class Login extends OpenBrowser {
	
		
		public void login() throws InterruptedException, BiffException, IOException {

  	  FileInputStream fileInputStream = new FileInputStream("Config.properties");
  	  Properties p = new Properties();
  	   	   p.load(fileInputStream);
  	   	   
  	  		String FileLocation = p.getProperty("TestCaseFileLocation");    	  		
  
		  //READING and GETTING DATA FROM EXCEL
		  
		  FileInputStream nxl = new FileInputStream(FileLocation);
		  jxl.Workbook nworkbook = jxl.Workbook.getWorkbook(nxl); 
		  jxl.Sheet nsheet = nworkbook.getSheet("Login"); 
		  int nrows = nsheet.getRows(); 
		  int ncolm = nsheet.getColumns(); 
		  String username =  nsheet.getCell(2, 2).getContents();  
		  String pwd= nsheet.getCell(3, 2).getContents();
		
					
				 PageFactory.initElements(driver, LoginPageElements.class);

				 driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS) ;
				 logger.info("Enterning user name");
			      LoginPageElements.username.sendKeys(username);
				 logger.info("Entering PWD");
			     LoginPageElements.password.sendKeys(pwd); 
				 LoginPageElements.loginButton.click();
				 logger.info("Login Success");
				 Thread.sleep(9000);
			
		
	
} 
    
    
	  @Test(priority = 1) 
	  public void ExecuteLogin() throws BiffException, IOException, InterruptedException, AWTException 
	  
{

	 	   FileInputStream fileInputStream = new FileInputStream("Config.properties");
	Properties p = new Properties();
	 	   p.load(fileInputStream);
	 	   
			String FileLocation2 = p.getProperty("TestCaseFileLocation");		
			
				
		  FileInputStream Exelogin = new FileInputStream(FileLocation2); 
		  jxl.Workbook ExeLogin_workbook = jxl.Workbook.getWorkbook(Exelogin); 
		  jxl.Sheet ELsheet = ExeLogin_workbook.getSheet("RunObjects"); 
		  int ELrows = ELsheet.getRows(); 
		  int ELcolm = ELsheet.getColumns(); 
	    
			String cellValue="1";
		
			if(cellValue.equals(ELsheet.getCell(2, 1).getContents()))
			{
			Login blogin = new Login();
				blogin.login();
				
				
			}
			else {
				System.out.println("skipped EL");
			}
		  
		  
	  }
	  
	  
	  

}
