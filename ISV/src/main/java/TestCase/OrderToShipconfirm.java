package TestCase;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import CommonFunctions.OpenBrowser;
import ObjectsandPageElements.Order_To_SM_PageElements;
import jxl.read.biff.BiffException;

public class OrderToShipconfirm extends OpenBrowser{
	

		public void OrderToSM () throws AWTException, InterruptedException, BiffException, IOException {
			  FileInputStream fileInputStream = new FileInputStream("Config.properties");
				Properties p = new Properties();
				 	   p.load(fileInputStream);
				 	   
						String FileLocation2 = p.getProperty("TestCaseFileLocation");
			
						 FileInputStream nxl = new FileInputStream(FileLocation2);
						  jxl.Workbook nworkbook = jxl.Workbook.getWorkbook(nxl); 
						  jxl.Sheet nsheet = nworkbook.getSheet("Login"); 
						  int nrows = nsheet.getRows(); 
						  int ncolm = nsheet.getColumns(); 
						  String username =  nsheet.getCell(2, 2).getContents();  
						  String pwd= nsheet.getCell(3, 2).getContents();

			  		  
			  PageFactory.initElements(driver, Order_To_SM_PageElements.class);
	
			  Thread.sleep(5000);
			  Order_To_SM_PageElements.app.click();
			  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  Order_To_SM_PageElements.appsearch.sendKeys("Orders");
			  Order_To_SM_PageElements.Orders.click();
			  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  Order_To_SM_PageElements.OrderNew.click();
			  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  Order_To_SM_PageElements.OrderDate.sendKeys("2/25/2022");
			  
			 
			  
			
		}
		@Test(priority = 1)
		public void ExeOrderToSM () throws IOException, BiffException, AWTException, InterruptedException {

		 	   FileInputStream fileInputStream = new FileInputStream("Config.properties");
		Properties p = new Properties();
		 	   p.load(fileInputStream);
		 	   
				String FileLocation2 = p.getProperty("TestCaseFileLocation");		
				
					
			  FileInputStream Exelogin = new FileInputStream(FileLocation2); 
			  jxl.Workbook ExeLogin_workbook = jxl.Workbook.getWorkbook(Exelogin); 
			  jxl.Sheet ELsheet = ExeLogin_workbook.getSheet("RunObjects"); 
			  int ELrows = ELsheet.getRows(); 
			  int ELcolm = ELsheet.getColumns(); 
		    
				String cellValue="1";
			
				if(cellValue.equals(ELsheet.getCell(2, 1).getContents()))
				{
				OrderToShipconfirm order = new 	OrderToShipconfirm();
					order.OrderToSM();
					
				}
				else {
					System.out.println("skipped ORDERTOSM");
				}
			  
			  
		  
		}

	}



