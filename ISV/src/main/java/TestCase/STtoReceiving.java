package TestCase;

import java.awt.AWTException;
import java.awt.Point;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import CommonFunctions.OpenBrowser;
import ObjectsandPageElements.AccountPageElements;
import ObjectsandPageElements.LoginPageElements;
import ObjectsandPageElements.po_elements;
import jxl.read.biff.BiffException;

public class STtoReceiving extends OpenBrowser {

	public void PO() throws AWTException, InterruptedException, BiffException, IOException {
		FileInputStream fileInputStream = new FileInputStream("Config.properties");
		Properties p = new Properties();
		p.load(fileInputStream);

		String FileLocation2 = p.getProperty("TestCaseFileLocation");

		FileInputStream nxl1 = new FileInputStream(FileLocation2);
		jxl.Workbook nworkbook1 = jxl.Workbook.getWorkbook(nxl1);
		jxl.Sheet nsheet1 = nworkbook1.getSheet(2);
		int nrows1 = nsheet1.getRows();
		int ncolm1 = nsheet1.getColumns();
		String accname = nsheet1.getCell(1, 1).getContents();

		String dba1 = nsheet1.getCell(1, 2).getContents();
		String ph1 = nsheet1.getCell(1, 3).getContents();
		

//		 Robot robot1 = new Robot();
//		  robot1.keyPress(KeyEvent.VK_ESCAPE);
//		  robot1.keyRelease(KeyEvent.VK_ESCAPE);;
//		  
//		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		  
////		  AccountPageElements.add.click();
		OpenBrowser ob = new OpenBrowser();

		PageFactory.initElements(driver, po_elements.class);

		// driver.get("https://bayview2-dev-ed.lightning.force.com/lightning/o/Account/list?filterName=Recent");
		Thread.sleep(5000);
		ob.clickOnTheElement(po_elements.applauncher);
		ob.clickOnTheElement(po_elements.applauncher_search);
		ob.inputValuestoTheElement(po_elements.applauncher_search, "service tickets");
		Thread.sleep(2000);
		ob.arrowkey_DOWN(po_elements.applauncher_search,3);
		ob.clickOnTheElement(po_elements.st_new);
		Thread.sleep(2000);
      ob.clickOnTheElement(po_elements.st_brndContact);
		
		/*String brc= ob.excelRead(0, 1);
		System.out.println(brc);
		ob.inputValuestoTheElement(po_elements.st_brndContact, brc);*/
		ob.arrowkey_DOWN(po_elements.st_brndContact,1);
		ob.clickOnTheElement(po_elements.st_brndName);
		ob.arrowkey_DOWN(po_elements.st_brndName,1);
		Thread.sleep(1000);
		ob.arrowkey_TAB(po_elements.st_transfermethod, 2);
		ob.clickOnTheElement(po_elements.st_transfermethod);
		ob.arrowkey_DOWN(po_elements.st_transfermethod,2);
		ob.clickOnTheElement(po_elements.st_accountname);
		ob.arrowkey_DOWN(po_elements.st_accountname,1);
		//ob.clickOnTheElement(po_elements.st_lp);
		ob.clickUsingActions(po_elements.st_lp);
		ob.arrowkey_DOWN(po_elements.st_lp,1);
		ob.clickOnTheElement(po_elements.save);
		
		ob.clickOnTheElement(po_elements.ST);
		/*ob.scrollbyUsingElement(po_elements.toc);
		ob.clickUsingActions(po_elements.toc);*/
		
		ob.arrowkey_TAB(po_elements.totalprice, 6);
	
	
	/*	WebElement ddlComparisons;
		static String[] runtimeConditions = ob.getDropDownOptions(po_elements.applauncher_search).toArray(new String[0]);
		// try {
		for (int i = 1; i <= runtimeConditions.length - 1; i++) {
	        System.out.println( runtimeConditions(i));
			//ob.clickOnTheElement(po_elements.Service_ticket);



			// Assert.assertEquals(actualConditions[i], runtimeConditions[i]);
		}*/

	}

	@Test(priority = 2)
	public void ExecreateAcc() throws IOException, BiffException, AWTException, InterruptedException {

		FileInputStream fileInputStream = new FileInputStream("Config.properties");
		Properties p = new Properties();
		p.load(fileInputStream);

		String FileLocation2 = p.getProperty("TestCaseFileLocation");

		FileInputStream Exelogin = new FileInputStream(FileLocation2);
		jxl.Workbook ExeLogin_workbook = jxl.Workbook.getWorkbook(Exelogin);
		jxl.Sheet ELsheet = ExeLogin_workbook.getSheet("RunObjects");
		int ELrows = ELsheet.getRows();
		int ELcolm = ELsheet.getColumns();

		String cellValue = "1";

		if (cellValue.equals(ELsheet.getCell(2, 1).getContents())) {
			STtoReceiving po = new STtoReceiving();
			po.PO();

		} else {
			System.out.println("skipped create acc");
		}

	}

}
