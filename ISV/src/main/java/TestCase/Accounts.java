package TestCase;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import CommonFunctions.OpenBrowser;
import ObjectsandPageElements.AccountPageElements;
import jxl.read.biff.BiffException;

public class Accounts extends OpenBrowser {
	
	public void createAcc () throws AWTException, InterruptedException, BiffException, IOException {
		  FileInputStream fileInputStream = new FileInputStream("Config.properties");
			Properties p = new Properties();
			 	   p.load(fileInputStream);
			 	   
					String FileLocation2 = p.getProperty("TestCaseFileLocation");
		
		 FileInputStream nxl1 = new FileInputStream(FileLocation2);
		  jxl.Workbook nworkbook1 = jxl.Workbook.getWorkbook(nxl1);
		  jxl.Sheet nsheet1 =  nworkbook1.getSheet(2); 
		  int nrows2 = nsheet1.getRows();
		  int ncolm1 =  nsheet1.getColumns(); 
		  String accname = nsheet1.getCell(2, 2).getContents();
		  
		  String dba1= nsheet1.getCell(2, 3).getContents();
		  String ph1= nsheet1.getCell(2, 4).getContents();


		  
//		 Robot robot1 = new Robot();
//		  robot1.keyPress(KeyEvent.VK_ESCAPE);
//		  robot1.keyRelease(KeyEvent.VK_ESCAPE);;
//		  
//		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		  
////		  AccountPageElements.add.click();
		  		  
		  PageFactory.initElements(driver, AccountPageElements.class);
		  driver.get("https://wovnpkg-dev-ed.lightning.force.com/lightning/o/Account/list?filterName=Recent");

		  AccountPageElements.naccount.click();
		  driver.manage().timeouts().implicitlyWait(5,  TimeUnit.SECONDS);
		  AccountPageElements.accountfor.click();
		  Thread.sleep(9000);
		  AccountPageElements.nextButton.click();
		  driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
		  AccountPageElements.accountname.sendKeys(accname);
		  AccountPageElements.DBA.sendKeys(dba1);
		  AccountPageElements.phone.sendKeys(ph1);
		  AccountPageElements.companyType.click(); 
		  AccountPageElements.typeRetailer.click();
		  AccountPageElements.Industry.click();
		  AccountPageElements.Industry_banking.click();
		 // AccountPageElements.SearchPricebook.sendKeys("Test");
		  //AccountPageElements.SelectPricebook.click();
		  AccountPageElements.SaveAsNew.click();
	}
	 @Test(priority = 2)
	public void ExecreateAcc () throws IOException, BiffException, AWTException, InterruptedException {

	 	   FileInputStream fileInputStream = new FileInputStream("Config.properties");
	Properties p = new Properties();
	 	   p.load(fileInputStream);
	 	   
			String FileLocation2 = p.getProperty("TestCaseFileLocation");		
			
				
		  FileInputStream Exelogin = new FileInputStream(FileLocation2); 
		  jxl.Workbook ExeLogin_workbook = jxl.Workbook.getWorkbook(Exelogin); 
		  jxl.Sheet ELsheet = ExeLogin_workbook.getSheet("RunObjects"); 
		  int ELrows = ELsheet.getRows(); 
		  int ELcolm = ELsheet.getColumns(); 
	    
			String cellValue="1";
		
			if(cellValue.equals(ELsheet.getCell(2, 1).getContents()))
			{
			Accounts acc = new Accounts();
				acc.createAcc();
				
			}
			else {
				System.out.println("skipped create acc");
			}
		  
		  
	  
	}

}
