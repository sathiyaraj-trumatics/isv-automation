package CommonFunctions;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestNG;
import org.testng.annotations.BeforeSuite;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class OpenBrowser {
	static TestNG testng;
	  public static WebDriver  driver;
   public static Logger logger= (Logger) Logger.getLogger(OpenBrowser.class);
   static Properties properties=null;
   public static  String FileLocations ="Uses/umashankard/Raj/Automation/AM.xls";
           
	 
	@BeforeSuite
	 public void OpenWeb () throws BiffException, IOException 
	 
	 {  
		
	   FileInputStream fileInputStream = new FileInputStream("Config.properties");
Properties p = new Properties();
	   p.load(fileInputStream);
	   
		String FileLocation = p.getProperty("TestCaseFileLocation");	
			DOMConfigurator.configure("Log4j.xml");
       
		 //... Read data and get from TestData Sheet..//
			
		  logger.info("Reading and getting data from sheet");
		  FileInputStream xl = new FileInputStream(FileLocation);
		  jxl.Workbook workbook = jxl.Workbook.getWorkbook(xl); 
        jxl.Sheet sheet = workbook.getSheet("CommonFunctions"); 
		   int nrows = sheet.getRows(); 
		  int ncolm = sheet.getColumns(); 
		  String link =  sheet.getCell(1, 5).getContents(); 
		  String ChromeLocation =sheet.getCell(1, 2).getContents(); 
		  
		  //... Launch Browser...//
		  logger.info("Launching Chrome Browser");
		  System.setProperty("webdriver.chrome.driver", ChromeLocation); 
		//Create a map to store  preferences 
		  Map<String, Object> prefs = new HashMap<String, Object>();

		  //add key and value to map as follow to switch off browser notification
		  //Pass the argument 1 to allow and 2 to block
		  prefs.put("profile.default_content_setting_values.notifications", 2);

		  //Create an instance of ChromeOptions 
		  ChromeOptions options = new ChromeOptions();

		  // set ExperimentalOption - prefs 
		  options.setExperimentalOption("prefs", prefs);

		  //Now Pass ChromeOptions instance to ChromeDriver Constructor to initialize chrome driver which will switch off this browser notification on the chrome browser
		   driver = new ChromeDriver(options);
		   driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	      	      

	        
	      //... Open Web URL...//  
	      
	        try {
	       logger.info("Opening Website"); 	
		  driver.navigate().to(link);
		  driver.manage().window().maximize();
    }
     
     catch (Exception e) {
   	logger.warn("Internet connect is required");
     }
	 
}
	public static void clickOnTheElement(WebElement element) {
		try {
		waitforElementVisibility(element);
		if (elementIsDisplayed(element)) {
		element.click();
		}
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new RuntimeException();

		}
		}


	
	
	public static void waitforElementVisibility(WebElement element) {
		try {
		WebDriverWait wd = new WebDriverWait(driver, 20);
		wd.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new RuntimeException();

		}
		}

	
	public static boolean elementIsDisplayed(WebElement element) {

		try {
		// WebElement element = driver.findElement(By.id(""));
		boolean displayed = element.isDisplayed();
		return displayed;
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new RuntimeException();
		}
		}
	

	
	public static void mouseHoverToTheElemnet(WebElement element) {
		try {
		waitforElementVisibility(element);
		Actions ac = new Actions(driver);
		ac.moveToElement(element).build().perform();
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new RuntimeException();
		}
		}
	
	public static List<String> getDropDownOptions(WebElement element){
        Select dd = new Select(element);
        List<WebElement> e = dd.getOptions();
        List<String> elemTexts = new ArrayList<>();
        for(WebElement opt:e){
            elemTexts.add(opt.getText());
        }
        return elemTexts;
    }

	
	public static boolean elementIsEnabled(WebElement element) {
		try {
		boolean enabled = element.isEnabled();
		return enabled;
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new RuntimeException();

		}
		}
	public static void arrowkey_DOWN(WebElement element, int value) {
		try {
			waitforElementVisibility(element);
			boolean elementIsDisplayed = elementIsDisplayed(element);
			boolean elementIsEnabled = elementIsEnabled(element);
			if (elementIsEnabled && elementIsDisplayed) {
				
				for(int i=1;i<=value;i++) {
					element.sendKeys(Keys.ARROW_DOWN);
				}
				Thread.sleep(1000);
				element.sendKeys(Keys.ENTER);
			
			}
			}
			 catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException();
					}
	}
	
	public static void inputValuestoTheElement(WebElement element, String value) {
		try {
		waitforElementVisibility(element);
		boolean elementIsDisplayed = elementIsDisplayed(element);
		boolean elementIsEnabled = elementIsEnabled(element);
		if (elementIsEnabled && elementIsDisplayed) {
		// element.clear();
		element.sendKeys(value);
		
		}
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		throw new RuntimeException();
		}
		}
	public static void arrowkey_TAB(WebElement element, int value) {
		try {
			waitforElementVisibility(element);
			boolean elementIsDisplayed = elementIsDisplayed(element);
			boolean elementIsEnabled = elementIsEnabled(element);
			if (elementIsEnabled && elementIsDisplayed) {
				
				for(int i=1;i<=value;i++) {
					element.sendKeys(Keys.TAB);
				}
				Thread.sleep(1000);
				//element.sendKeys(Keys.ENTER);
			
			}
			}
			 catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException();
					}
	}
	public static void scrollbyCoordinates(int height, int width) {
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("windows.scrollBy("+width+","+height+")");
		} catch (Exception e) {
		throw new RuntimeException("Unable to Scroll");
		}
	}
	public static void scrollbyUsingElement(WebElement element) {
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView()", element);
		} catch (Exception e) {
		throw new RuntimeException("Unable to Scroll");
		}
		}
	public static void clickUsingActions(WebElement element) {
		try {
		Actions ac = new Actions(driver);
		ac.click(element).build().perform();;
		} catch (Exception e) {
		throw new RuntimeException("Unable to sendKeysUsingActions");
		}

		
		}
}


