package ObjectsandPageElements;



import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import CommonFunctions.OpenBrowser;
import jxl.read.biff.BiffException;

public class LoginPageElements extends OpenBrowser{

	 
		@FindBy(id="username")
		public static WebElement username;
		@FindBy(id="password")
		public static WebElement password;
		@FindBy(id="Login")
		public static WebElement loginButton;
			
	
}


