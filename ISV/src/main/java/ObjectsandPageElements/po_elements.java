package ObjectsandPageElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class po_elements {
	@FindBy(xpath = "//div[@class='slds-icon-waffle']")
	public static WebElement applauncher;
	@FindBy(xpath="//input[@placeholder='Search apps and items...']")
	public static WebElement applauncher_search;
	@FindBy(xpath="//h3[text()='Apps']")
	public static WebElement apps;
	//@FindBy(xpath= "//a[contains(@href,'lightning/o/filigreenconnec__Service_Ticket__c/home')]")
	@FindBy(xpath="//b[text()='Service Ticket']")
	public static WebElement Service_ticket;
	@FindBy(xpath="//div[text()='New']")
	public static WebElement st_new;
	
	
	@FindBy(xpath = "//label[text()='Brand Contact']//../div/div/lightning-base-combobox/div/div/input")
	public static WebElement st_brndContact;
	@FindBy(xpath = "//label[text()='Brand Name']//../div/div/lightning-base-combobox/div/div/input")
	public static WebElement st_brndName;
	@FindBy(xpath = "//label[text()='Transfer Method']//../div/lightning-base-combobox/div/div/button")
	public static WebElement st_transfermethod;
	@FindBy(xpath = "//label[text()='Account Name']//../div/div/lightning-base-combobox/div/div/input")
	public static WebElement st_accountname;
	@FindBy(xpath="//label[text()='Licensed Premise']//../div/div/lightning-base-combobox/div/div/input")
	public static WebElement st_lp;
	
	
	@FindBy(xpath="//button[text()='Save']")
	public static WebElement save;
	
	@FindBy(xpath="//button[text()='New']")
	public static WebElement ST;
	
	@FindBy(xpath = "//textarea[@maxlength='1500']")
	public static WebElement spn ;
	
	@FindBy(xpath = "filigreenconnec__Transfer_of_Custody__c")
	public static WebElement toc;
	
	@FindBy(xpath = "//input[@name='filigreenconnec__Total_Quoted_Price__c']")
	public static WebElement totalprice;
	

}
