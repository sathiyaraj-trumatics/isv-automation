package ObjectsandPageElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Order_To_SM_PageElements {
	
	@FindBy(xpath="/html/body/div[4]/div[1]/section/div[1]/div[1]/one-appnav/div/div/div/div/one-app-launcher-header/button/div")
	public static WebElement app;
	@FindBy(xpath="//input[@placeholder='Search apps and items...']")
	public static WebElement appsearch;
    @FindBy(xpath="//b[text()='Orders']")
	public static WebElement Orders;
	@FindBy(xpath="//div[text()='New']")
	public static WebElement OrderNew;
	@FindBy(xpath="//*[@id=\"input-220\"]")
	public static WebElement OrderDate;
	
}
	
