package ObjectsandPageElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountPageElements {

		
	public static WebElement applauncher;
	@FindBy(xpath="//*[@class='slds-icon-waffle']")
	public static WebElement applauncher1;
	@FindBy(xpath="//button[text()=\"View All\"]")
	public static WebElement viewall;
	@FindBy(xpath="//button[text()=\"View All\"]")
	public static WebElement viewall1;
	@FindBy(xpath="//p[text()=\"Filigreen Sales\"]")
	public static WebElement fgsales;
	@FindBy(xpath="//*[@class='loginInputBox input wide focus mt8']")
	public static WebElement skip;
	@FindBy(linkText="Accounts")
	public static WebElement accounts;
	@FindBy(xpath="//*[@role='listitem'][2]")
	public static WebElement add;
	
	
	@FindBy(xpath="//*[@class='slds-button slds-button--neutral slds-button slds-button_neutral uiButton']")
	public static WebElement cancel;
	@FindBy(xpath="//*[@class='slds-col slds-no-flex slds-grid slds-align-top slds-p-bottom--xx-small test-lvmForceActionsContainer']/ul/li[1]")
	public static WebElement naccount;
	@FindBy(xpath="//*[@class='changeRecordTypeRightColumn slds-form-element__control']/div[3]")
	public static WebElement accountfor;	
	@FindBy(xpath="//*[text()=\"Next\"]")
	public static WebElement nextButton;
	@FindBy(xpath="//div[@class='autocompleteWrapper slds-grow']/input")
	public static WebElement accountname;
	@FindBy(xpath="//*[@class=' textarea']")
	public static WebElement DBA;
	@FindBy(xpath="(//*[@placeholder=\"\"])[4]")
	public static WebElement phone;
	@FindBy(xpath="//*[text()=\"--None--\"]")
	public static WebElement pay_Term;
	@FindBy(xpath="//*[@title='NET 5']")
	public static WebElement pay_net5;
	
	//Company Profile//
	
	@FindBy(xpath="//*[text()=\"Company Profile\"]/following::*[text()=\"--None--\"]")
	public static WebElement companyType;
	@FindBy(xpath="//a[@title='Retailer']")
	public static WebElement typeRetailer;	
	@FindBy(xpath="//*[text()=\"Industry\"]/following::*[text()=\"--None--\"]")
	public static WebElement Industry;
	@FindBy(xpath="//*[@title='Banking']")
	public static WebElement Industry_banking;
		@FindBy(xpath="//span[@title='Distributor']")
	public static WebElement licenseSelect;
		@FindBy(xpath="//*[@title='Move selection to Chosen']")
		public static WebElement licenseAdd;
		
		//Deleivery Timings//
		
		//@FindBy(xpath="//*[@name='Delivery Earliest Time']")
		//public static WebElement startTime;
		//@FindBy(xpath="//*[@name='Delivery Latest Time']")
		//public static WebElement LastTime;
		
		
		//Address//		
		
		@FindBy(xpath="//*[text()=\"Billing Address\"]/following::*[text()=\"Search Address\"]")
		public static WebElement BillingAddressSearch;		
		@FindBy(xpath="//*[@placeholder='Enter address']")
		public static WebElement EnterBillingAddress;
		
		
		@FindBy(xpath="//*[@placeholder='Billing Street']")
		public static WebElement EnterBillingStreet;
		
		@FindBy(xpath="//*[text()=\"Shipping Address\"]/following::*[text()=\"Search Address\"]")
		public static WebElement ShippingAddressSearch;
		@FindBy(xpath="//*[@placeholder='Enter address']")
		public static WebElement EnterShippingAddress;
		
		@FindBy(xpath="//*[@placeholder='Shipping Street']")
		public static WebElement EnterShippingStreet;
		
		//Community details
		
		//@FindBy(xpath="//*[@placeholder='Search Price Books...']")
		//public static WebElement SearchPricebook;
		//@FindBy(xpath="//*[@class='data-match'][text()=\"Test\"]")
		//public static WebElement SelectPricebook;
		
		//@FindBy(xpath="//*[text()=\"Community Details\"]/following::*[text()=\"--None--\"]")
		//public static WebElement EmergingBrandOrder;
		//@FindBy(xpath="//*[@title='2']")
		//public static WebElement SelectBrandOrder;
		
		@FindBy(xpath="//*[text()=\"Total Community License\"]/following::*[@type='text']")
		public static WebElement totalCommunityLicense;
		//@FindBy(xpath="//*[text()=\"Facebook\"]/following::input[1]")
		//public static WebElement facebook;
		
		//@FindBy(xpath="//*[text()=\"Twitter\"]/following::input[1]")
		//public static WebElement twitter;
		
		//@FindBy(xpath="//*[text()=\"Instagram\"]/following::input[1]")
		//public static WebElement instagram;
		
		//@FindBy(xpath="//*[text()=\"LinkedIn\"]/following::input[1]")
		//public static WebElement LinkedIn;
		
		
		@FindBy(xpath="//*[text()=\"About Us\"]/following::div[18]")
		public static WebElement about;
		
		
		@FindBy(xpath="//*[@title='Save']")
		public static WebElement saveButton;
		
		
		@FindBy(xpath="//*[@title='Save & New']")
		public static WebElement SaveAsNew;
	}

